'use strict';

// fetch all the sections
var sections = document.querySelectorAll('.js-section');
var content = document.querySelector('main');
var body = document.querySelector('body');

// On scroll check what section is in view
document.addEventListener('scroll', function () {

    // Main distance from the top border
    var mainToTop = content.getBoundingClientRect().top;
    var counter = 0;
    for (var i = 0; i < sections.length; i++) {
        var rect = sections[i].getBoundingClientRect();
        var top = rect.top - (window.innerHeight || document.documentElement.clientHeight) / 2 < 0;
        var bottom = rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) / 2;
        var result = top && !bottom;
        sections[i].classList.remove('in-view');
        if (result) {
            sections[i].classList.add('in-view');
            // Fetch data and assign class to body
            var color = sections[i].dataset.color;
            body.setAttribute('data-color', color);
        }
    }
    if (document.querySelector('.in-view') == null) {
        body.setAttribute('data-color', 'yellow');
    }
});